<?php

$dictionary['Project']['fields']['amount_used_c'] = array (
	'name' => 'amount_used_c',
	'vname' => 'LBL_AMOUNT_USED_C',
	'type' => 'currency',
	'dbType' => 'double',
	'comment' => 'Cost',
	'importable' => 'required',
	'duplicate_merge'=>'1',
	'required' => true,
	'options' => 'numeric_range_search_dom',
	'enable_range_search' => true,
);

?>