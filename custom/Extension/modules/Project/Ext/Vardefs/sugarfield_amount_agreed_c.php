<?php

$dictionary['Project']['fields']['amount_agreed_c'] = array (
	'name' => 'amount_agreed_c',
	'vname' => 'LBL_AMOUNT_AGREED_C',
	'type' => 'currency',
	'dbType' => 'double',
	'comment' => 'Cost',
	'importable' => 'required',
	'duplicate_merge'=>'1',
	'required' => true,
	'options' => 'numeric_range_search_dom',
	'enable_range_search' => true,
);

?>