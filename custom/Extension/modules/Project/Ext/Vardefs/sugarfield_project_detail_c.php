<?php
$dictionary['Project']['fields']['project_detail_c'] = array (
  'name' => 'project_detail_c',
  'vname' => 'LBL_PROJECT_DETAIL_C',
  'type' => 'varchar',
  'len' => '100',
  'comment' => 'Custom field for project',
);
?>