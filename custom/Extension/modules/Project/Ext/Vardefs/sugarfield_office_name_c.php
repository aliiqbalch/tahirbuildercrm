<?php
$dictionary['Project']['fields']['office_name_c'] = array (
  'name' => 'office_name_c',
  'vname' => 'LBL_OFFICE_NAME_C',
  'type' => 'varchar',
  'len' => '100',
  'comment' => 'Custom field for project',
);
?>