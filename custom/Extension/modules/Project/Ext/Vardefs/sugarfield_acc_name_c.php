<?php
$dictionary['Project']['fields']['acc_name_c'] = array (
  'name' => 'acc_name_c',
  'vname' => 'LBL_ACC_NAME_C',
  'type' => 'varchar',
  'len' => '100',
  'comment' => 'Custom field for project',
);
?>