<?php
$dictionary['Project']['fields']['days_left_c'] = array (
  'name' => 'days_left_c',
  'vname' => 'LBL_DAYS_LEFT_C',
  'type' => 'varchar',
  'len' => '100',
  'comment' => 'Custom field for project',
);
?>