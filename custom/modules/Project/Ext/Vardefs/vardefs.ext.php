<?php 
 //WARNING: The contents of this file are auto-generated


$dictionary['Project']['fields']['acc_name_c'] = array (
  'name' => 'acc_name_c',
  'vname' => 'LBL_ACC_NAME_C',
  'type' => 'varchar',
  'len' => '100',
  'comment' => 'Custom field for project',
);



$dictionary['Project']['fields']['amount_agreed_c'] = array (
	'name' => 'amount_agreed_c',
	'vname' => 'LBL_AMOUNT_AGREED_C',
	'type' => 'currency',
	'dbType' => 'double',
	'comment' => 'Cost',
	'importable' => 'required',
	'duplicate_merge'=>'1',
	'required' => true,
	'options' => 'numeric_range_search_dom',
	'enable_range_search' => true,
);




$dictionary['Project']['fields']['amount_left_c'] = array (
	'name' => 'amount_left_c',
	'vname' => 'LBL_AMOUNT_LEFT_C',
	'type' => 'currency',
	'dbType' => 'double',
	'comment' => 'Cost',
	'importable' => 'required',
	'duplicate_merge'=>'1',
	'required' => true,
	'options' => 'numeric_range_search_dom',
	'enable_range_search' => true,
);




$dictionary['Project']['fields']['amount_used_c'] = array (
	'name' => 'amount_used_c',
	'vname' => 'LBL_AMOUNT_USED_C',
	'type' => 'currency',
	'dbType' => 'double',
	'comment' => 'Cost',
	'importable' => 'required',
	'duplicate_merge'=>'1',
	'required' => true,
	'options' => 'numeric_range_search_dom',
	'enable_range_search' => true,
);




$dictionary['Project']['fields']['currency_id_c'] = array (
    'name' => 'currency_id',
    'type' => 'id',
    'group'=>'currency_id',
    'vname' => 'LBL_CURRENCY',
	'function'=>array('name'=>'getCurrencyDropDown', 'returns'=>'html'),
    'reportable'=>false,
    'comment' => 'Currency used for display purposes'
  );




$dictionary['Project']['fields']['currency_name_c'] = array(
		'name'=>'currency_name',
		'rname'=>'name',
		'id_name'=>'currency_id',
		'vname'=>'LBL_CURRENCY_NAME',
		'type'=>'relate',
		'isnull'=>'true',
		'table' => 'currencies',
		'module'=>'Currencies',
		'source' => 'non-db',
        'function'=>array('name'=>'getCurrencyNameDropDown', 'returns'=>'html'),
        'studio' => 'false',
   	    'duplicate_merge' => 'disabled',
);




$dictionary['Project']['fields']['currency_symbol_c'] = array(
		'name'=>'currency_symbol',
		'rname'=>'symbol',
		'id_name'=>'currency_id',
		'vname'=>'LBL_CURRENCY_SYMBOL',
		'type'=>'relate',
		'isnull'=>'true',
		'table' => 'currencies',
		'module'=>'Currencies',
		'source' => 'non-db',
        'function'=>array('name'=>'getCurrencySymbolDropDown', 'returns'=>'html'),
        'studio' => 'false',
   	    'duplicate_merge' => 'disabled',
);



$dictionary['Project']['fields']['days_left_c'] = array (
  'name' => 'days_left_c',
  'vname' => 'LBL_DAYS_LEFT_C',
  'type' => 'varchar',
  'len' => '100',
  'comment' => 'Custom field for project',
);


 // created: 2017-12-12 12:09:22
$dictionary['Project']['fields']['jjwg_maps_address_c']['inline_edit']=1;

 

 // created: 2017-12-12 12:09:22
$dictionary['Project']['fields']['jjwg_maps_geocode_status_c']['inline_edit']=1;

 

 // created: 2017-12-12 12:09:21
$dictionary['Project']['fields']['jjwg_maps_lat_c']['inline_edit']=1;

 

 // created: 2017-12-12 12:09:21
$dictionary['Project']['fields']['jjwg_maps_lng_c']['inline_edit']=1;

 

$dictionary['Project']['fields']['office_name_c'] = array (
  'name' => 'office_name_c',
  'vname' => 'LBL_OFFICE_NAME_C',
  'type' => 'varchar',
  'len' => '100',
  'comment' => 'Custom field for project',
);



$dictionary['Project']['fields']['progress_status_c'] = array (
'name' => 'progress_status_c',
'vname' => 'LBL_PROGRESS_STATUS_C',
'type' => 'enum',
'options' => 'progress_status_c_dom',

);


$dictionary['Project']['fields']['project_detail_c'] = array (
  'name' => 'project_detail_c',
  'vname' => 'LBL_PROJECT_DETAIL_C',
  'type' => 'varchar',
  'len' => '100',
  'comment' => 'Custom field for project',
);

?>